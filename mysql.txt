1. Soal 1 Membuat Database
   create database myshop;


2.Soal 2 Membuat Tabel di dalam Database
table users
create table users(
    -> id int(10) primary key auto_increment,
    -> name varchar(255),
    -> email varchar(255),
    -> password varchar(255)
    -> );

table categories
create table categories(
    -> id int(10) primary key auto_increment,
    -> name varchar(255)
    -> );

table items
create table items(
    -> id int(10) primary key auto_increment,
    -> name varchar(255),
    -> description varchar(255),
    -> price int(20),
    -> stock int(20),
    -> category_id int(20),
    -> foreign key(category_id) references categories(id)
    -> );


3. Soal 3 Memasukkan Data pada Tabel
tabel users
insert into users(name, email, password) values("John Doe","john@doe.com","john123");
insert into users(name, email, password) values("Jane Doe","jane@doe.com","jenita123");

tabel categories
insert into categories(name) values("gadget");
insert into categories(name) values("cloth");
insert into categories(name) values("men");
insert into categories(name) values("women");
insert into categories(name) values("branded");
atau bisa juga dengan
insert into categories(name) values("gadget"),("cloth"),("men"),("women"),("branded");

tabel items
insert into items(name,description,price,stock,category_id) values("Sumsang b50","hape keren dari merek sumsang",4000000,100,1);
insert into items(name,description,price,stock,category_id) values("Uniklooh","baju keren dari brand ternama",500000,50,2);
insert into items(name,description,price,stock,category_id) values("IMHO Watch","jam tangan anak yang jujur banget",2000000,10,1);


4. Soal 4 Mengambil Data dari Database 
a. Mengambil data users
   select id,name,email from users;

b. Mengambil Data Items
   select * from items where price>1000000;
   select * from items where name like '%watch';

c. Menampilkan data items join dengan kategori
   select items.name, items.description, items.price, items.stock, items.category_id, categories.name as kategori from items inner join categories on items.category_id = categories.id;


5. Soal 5 Mengubah Data dari Database
   update items set price=2500000 where id=1;

